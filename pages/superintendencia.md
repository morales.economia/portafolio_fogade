---
title: Superintendencia
slug: superintendencia
date: 2012-09-15 19:52:05 UTC
---

La nueva página de la Superintendencia de Bancos y de Otras Instituciones Financieras de Nicaragua es un laberinto sin salida cuando se trata de obtener los últimos informes financieros que publica acerca de la situación bancaria del país. Por eso considero un buen aporte de mi parte recolectarlos en esta página.

--------------------

##### **Balance de Situación en Perspectiva**
Agosto 2018 - [Balance de Situación en Perspectiva](http://www.superintendencia.gob.ni/sites/default/files/documentos/informes/bancos/balgp201808sfb.htm)

Julio 2018 - [Balance de Situación en Perspectiva](http://www.superintendencia.gob.ni/sites/default/files/documentos/informes/bancos/balgp201807sfb.htm)

Junio 2018 - [Balance de Situación en Perspectiva](http://www.superintendencia.gob.ni/sites/default/files/documentos/informes/bancos/balgp201806sfb.htm)

-----------------------

##### **Estado de Resultado en Perspectiva**
Agosto 2018 - [Estado de Resultado en Perspectiva](http://www.superintendencia.gob.ni/sites/default/files/documentos/informes/bancos/edorp201808sfb.htm)

Julio 2018 - [Estado de Resultado en Perspectiva](http://www.superintendencia.gob.ni/sites/default/files/documentos/informes/bancos/edorp201807sfb.htm)

Junio 2018 - [Estado de Resultado en Perspectiva](http://www.superintendencia.gob.ni/sites/default/files/documentos/informes/bancos/edorp201806sfb.htm)

----------------------------

##### **Indicadores Financieros Comparativos**
Julio 2018 - [Indicadores Financieros Comparativos](http://www.superintendencia.gob.ni/sites/default/files/documentos/informes/bancos/inf201807sfn.htm)

Junio 2018 - [Indicadores Financieros Comparativos](http://www.superintendencia.gob.ni/sites/default/files/documentos/informes/bancos/inf201806sfn.htm)

-------------------------------

##### **Cálculo de Adecuación de Capital (Trimestral)**
Junio 2018 - [Cálculo de Adecuación de Capital](http://www.superintendencia.gob.ni/sites/default/files/documentos/informes/bancos/adec201706sfb.htm)

--------------------------------

##### **Estratificacion de los Depósitos por Monto y Modalidad (Trimestral)**
Junio 2018 - [Estratificacion de los Depósitos por Monto y Modalidad](http://www.superintendencia.gob.ni/sites/default/files/documentos/informes/bancos/edmm201806sfb.htm)

---------------------------------

##### **Estratificación de Depósitos por Monto y Subsistemas (Trimestral)**
Junio 2018 - [Estratificación de Depósitos por Monto y Subsistemas](http://www.superintendencia.gob.ni/sites/default/files/documentos/informes/bancos/edms201806sfb.htm)

---------------------------------

##### **Estratificación de la Cartera Total por Monto y Plazo Contractual (Trimestral)**
Junio 2018 - [Estratificación de la Cartera Total por Monto y Plazo Contractual](http://www.superintendencia.gob.ni/sites/default/files/documentos/informes/bancos/ectmpc201806sfb.htm)

---------------------------------

##### **Estratificación de la Cartera por Actividad y Monto (Trimestral)**
Junio 2018 - [Estratificación de la Cartera por Actividad y Monto](http://www.superintendencia.gob.ni/sites/default/files/documentos/informes/bancos/ecam201806sfb.htm)
