---
title: "Depósitos Asegurados Julio 2018"
author: "Deybi Morales"
date: '2018-08-27'
slug: depositos-asegurados-Julio2018
---

<!--This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code.-->

<!--Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*.-->

<!--
#```{r}
plot(cars)
#```-->

<!--
Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).
-->

Actualización: Julio 2018

Reporte Presentado al Consejo Directivo:

<iframe src="https://campuen-my.sharepoint.com/:p:/g/personal/mic621033_365office_site/EUT1qhJ4qhNErdzkYs8ljhsBlIBtdc3f0ZpEOjzO21kajQ?e=QO9621}&amp;action=embedview&amp;wdAr=1.7777777777777777" width="962px" height="565px" frameborder="0">Esto es un documento de <a target="_blank" href="https://office.com">Microsoft Office</a> incrustado con tecnología de <a target="_blank" href="https://office.com/webapps">Office Online</a>.</iframe>


[DESCARGAR](https://campuen-my.sharepoint.com/:p:/g/personal/mic621033_365office_site/EUT1qhJ4qhNErdzkYs8ljhsBlIBtdc3f0ZpEOjzO21kajQ?e=QO9621)
