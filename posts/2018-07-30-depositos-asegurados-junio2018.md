---
title: "Depósitos Asegurados Junio 2018"
author: "Deybi Morales"
date: '2018-07-30'
slug: depositos-asegurados-Junio2018
---

<!--This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code.-->

<!--Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*.-->

<!--
#```{r}
plot(cars)
#```-->

<!--
Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).
-->

Actualización: Junio 2018

Reporte Presentado al Consejo Directivo:

<iframe src="https://campuen-my.sharepoint.com/:p:/g/personal/mic621033_365office_site/ETs5m1GxeJJPpHpeIr7WcUAByN5ply3xK3BdhGZMVLFy7A?e=JxGRly}&amp;action=embedview&amp;wdAr=1.7777777777777777" width="962px" height="565px" frameborder="0">Esto es un documento de <a target="_blank" href="https://office.com">Microsoft Office</a> incrustado con tecnología de <a target="_blank" href="https://office.com/webapps">Office Online</a>.</iframe>


[DESCARGAR](https://campuen-my.sharepoint.com/:p:/g/personal/mic621033_365office_site/ETs5m1GxeJJPpHpeIr7WcUAByN5ply3xK3BdhGZMVLFy7A?e=JxGRly)
